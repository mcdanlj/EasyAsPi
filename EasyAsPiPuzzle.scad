// https://www.mathsisfun.com/puzzles/as-easy-as-pi.html
// Spoiler: https://www.mathsisfun.com/puzzles/as-easy-as-pi-solution.html
// This model Copyright Michael K Johnson, CC0 License
// https://creativecommons.org/share-your-work/public-domain/cc0/
//
// Rearrange the pieces only by sliding them, without flipping them over, to make a square

// Height in mm
h = 4; // [1:10]
// X/Y base unit square size in mm; completed puzzle four by four base units
s = 25; // [5:100]

module one() {
    p = [[0, 2*s], [0, 3*s], [3*s, 3*s], [1*s, 1*s], [1*s, 2*s]];
    color("red") linear_extrude(h) polygon(points=p);
}
module two() {
    p = [[2*s, 2*s], [3*s, 3*s], [5*s, 3*s], [3*s, 1*s], [3*s, 2*s]];
    color("green") linear_extrude(h) polygon(points=p);
}
module three() {
    p = [[4*s, 2*s], [5*s, 3*s], [5*s, 2*s]];
    color("blue") linear_extrude(h) polygon(points=p);
}
module four() {
    p = [[1*s, 0*s], [1*s, 1*s], [2*s, 2*s], [2*s, 0*s]];
    color("gold") linear_extrude(h) polygon(points=p);
}
module five() {
    p = [[3*s, 0*s], [3*s, 1*s], [4*s, 2*s], [4*s, 0*s]];
    color("silver") linear_extrude(h) polygon(points=p);
}
module puzzle() {
    // Separate the parts by 2mm for printing
    translate([0, 2, 0]) one();
    translate([2, 2, 0]) two();
    translate([4, 2, 0]) three();
    four();
    translate([2, 0, 0]) five();
}
// To print, render the puzzle and export to STL or AMF
puzzle();
// To laser cut, comment out the line above, uncomment the line below, and render the puzzle and export to DXF or SVG
//projection(cut=false) puzzle();
